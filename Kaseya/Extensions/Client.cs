﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace Kaseya
{
    public partial class Client
    {
        public TokenInfo Token { get; private set; }

        private string user = "dashboard";
        private string password = "dash2014Kaseya7";

        public string Authorization => $"Bearer {Token.Token}";

        private Client() { }

        public static async Task<Client> CreateInstance(HttpClient httpClient)
        {
            var client = new Client();
            client._httpClient = httpClient;
            client._settings = new System.Lazy<JsonSerializerSettings>(client.CreateSerializerSettings);
            var key = new Hash(client.user, client.password).GetKey();
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", key);
            await client.UpdateToken(client.user, client.password);
            return client;
        }

        public static async Task<Client> CreateInstance(HttpClient httpClient, string userName, string userPassword)
        {
            var client = new Client();
            client._httpClient = httpClient;
            client._settings = new System.Lazy<Newtonsoft.Json.JsonSerializerSettings>(client.CreateSerializerSettings);
            var key = new Hash(userName, userPassword).GetKey();
            httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", key);
            client.user = userName;
            client.password = userPassword;
            await client.UpdateToken(userName, userPassword);
            return client;
        }

        public async Task UpdateToken(string userName, string userPassword)
        {
            var key = new Hash(userName, userPassword).GetKey();
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", key);
            var tokenInfo = JsonConvert.SerializeObject((await Auth_GetSessionAsync(userName, userPassword, string.Empty)).Result);
            Token = JsonConvert.DeserializeObject<TokenInfo>(tokenInfo);
        }

        public async Task UpdateToken()
        {
            var key = new Hash(user, password).GetKey();
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", key);
            var tokenInfo = JsonConvert.SerializeObject((await Auth_GetSessionAsync(user, password, string.Empty)).Result);
            Token = JsonConvert.DeserializeObject<TokenInfo>(tokenInfo);
        }
    }
}
