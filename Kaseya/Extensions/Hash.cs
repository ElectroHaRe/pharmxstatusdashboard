﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Kaseya
{
    internal class Hash
    {
        public string RandomNumber { get; protected set; }
        public string RawSHA256Hash { get; protected set; }
        public string CoveredSHA256Hash { get; protected set; }
        public string RawSHA1Hash { get; protected set; }
        public string CoveredSHA1Hash { get; protected set; }
        private string userName { get; set; }

        public Hash(string userName, string password)
        {
            RandomNumber = GenerateRandomNumber(8);
            RawSHA256Hash = CalculateHash(password, "SHA-256");
            CoveredSHA256Hash = CalculateHash(password, userName, "SHA-256");
            CoveredSHA256Hash = CalculateHash(CoveredSHA256Hash, RandomNumber, "SHA-256");
            RawSHA1Hash = CalculateHash(password, "SHA-1");
            CoveredSHA1Hash = CalculateHash(password, userName, "SHA-1");
            CoveredSHA1Hash = CalculateHash(CoveredSHA1Hash, RandomNumber, "SHA-1");
            this.userName = userName;
        }

        private string CalculateHash(string Value1, string Value2, string HashingAlgorithm)
        {
            return CalculateHash(Value1 + Value2, HashingAlgorithm);
        }

        private string CalculateHash(string Value, string HashingAlgorithm)
        {
            byte[] arrByte;
            if (HashingAlgorithm == "SHA-1")
            {
                SHA1Managed hash = new SHA1Managed();
                arrByte = hash.ComputeHash(ASCIIEncoding.ASCII.GetBytes(Value));
            }
            else if (HashingAlgorithm == "SHA-256")
            {
                SHA256Managed hash = new SHA256Managed();
                arrByte = hash.ComputeHash(ASCIIEncoding.ASCII.GetBytes(Value));
            }
            else
            {
                throw new System.ApplicationException(string.Format("Unknow hashing algorithm: {0}", HashingAlgorithm));
            }
            string s = "";
            for (int i = 0; i < arrByte.Length; i++)
            {
                s += arrByte[i].ToString("x2");
            }
            return s;
        }

        private string GenerateRandomNumber(int numberOfDigits)
        {
            System.Security.Cryptography.RNGCryptoServiceProvider rng = new System.Security.Cryptography.RNGCryptoServiceProvider();
            byte[] numbers = new byte[numberOfDigits * 2];
            rng.GetNonZeroBytes(numbers);
            string result = "";
            for (int i = 0; i < numberOfDigits; i++)
            {
                result += numbers[i].ToString();
            }
            result = result.Replace("0", "");
            return result.Substring(1, numberOfDigits);
        }

        public string GetKey()
        {
            return Convert.ToBase64String(
                        Encoding.Default.GetBytes(
                            string.Format("user={0},pass2={1},pass1={2},rpass2={3},rpass1={4},rand2={5}",
                                userName,
                                CoveredSHA256Hash,
                                CoveredSHA1Hash,
                                RawSHA256Hash,
                                RawSHA1Hash,
                                RandomNumber)));
        }
    }
}
