﻿using System;

namespace Kaseya
{
    public struct TokenInfo
    {
        public string Token { get; set; }

        /*public string TenantId { get; set; }
        public string UserName { get; set; }
        public int RoleId { get; set; }
        public string ScopeId { get; set; }
        public int AdminId { get; set; }
        public string MachineId { get; set; }
        public string MachineGroupName { get; set; }*/

        public DateTime SessionExpiration { get; set; }

        /*public string CultureInfo { get; set; }
        public string TimePref { get; set; }
        public int OffSetInMinutes { get; set; }
        public string ApiToken { get; set; }
        public string CorrelationId { get; set; }
        public int LanguageId { get; set; }
        public string Attributes { get; set; }*/
    }
}
