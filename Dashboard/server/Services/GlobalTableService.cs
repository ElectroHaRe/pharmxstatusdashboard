﻿using Kaseya;
using PharmXStatusDashboard.BaseAbstractions;
using PharmXStatusDashboard.TableEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PharmXStatusDashboard.Services
{
    public class GlobalTableService : TableService
    {
        private const decimal PharmXView = 85862106M;
        private const decimal CleverenceInstalledView = 96429741M;

        public List<Organization> OrgTable { get; protected set; } = new List<Organization>();
        public List<PharmX> PharmXTable { get; protected set; } = new List<PharmX>();
        public List<TSD> TsdTable { get; protected set; } = new List<TSD>();
        public List<Trouble> TroubleTable { get; protected set; } = new List<Trouble>();
        public List<PharmX> PharmXReleaseTable { get; protected set; } = new List<PharmX>();
        public List<PharmX> PharmXTestTable { get; protected set; } = new List<PharmX>();
        public List<TSD> TsdReleaseTable { get; protected set; } = new List<TSD>();
        public List<TSD> TsdTestTable { get; protected set; } = new List<TSD>();

        public GlobalTableService(TimeSpan updateInterval) : base(updateInterval)
        { }

        protected override async Task<List<ITableEntity>> GenerateTable()
        {
            var orgs = await GetOrgCollection();
            var tsd = await GetTsdCollection();
            var phx = await GetPhxCollection();

            foreach (Organization org in orgs)
            {
                org.PharmX.AddRange(GetPharmXRange(org.PhxUrls, phx).Cast<PharmX>());
                org.TSD.AddRange(GetTSDRange(org.TsdUrls, tsd).Cast<TSD>());

                foreach (var item in org.PharmX)
                {
                    item.Organization = org;
                }

                foreach (var item in org.TSD)
                {
                    item.Organization = org;
                }
            }

            var troubles = GetTroubleCollection();

            var result = orgs;
            result.AddRange(tsd);
            result.AddRange(phx);
            result.AddRange(troubles);

            return result;
        }

        protected async Task<List<ITableEntity>> GetOrgCollection()
        {
            var table = new List<ITableEntity>();
            var orgs = await client.Org_GetOrgsAsync(client.Authorization, filter: "startswith(OrgRef, 'trace-x.')");
            string[] range;

            foreach (var org in orgs.Result)
            {
                var entity = new Organization();

                entity.ShortName = org.OrgName;
                entity.TSDType =
                    org?.CustomFields.FirstOrDefault((item) => item.FieldName == "Support Level")?.FieldValue ?? "unknown";

                entity.PhxUrls = org.CustomFields.FirstOrDefault((item) => item.FieldName == "Contact Details 1")?.FieldValue;
                entity.TsdUrls = org.CustomFields.FirstOrDefault((item) => item.FieldName == "Contact Details 2")?.FieldValue;

                var data = org?.CustomFields.FirstOrDefault((item) => item.FieldName == "Training")?.FieldValue;
                if (!string.IsNullOrEmpty(data))
                {
                    entity.FullName = Regex.Match(data, @"(?<=Наименование организации:)\s*.*\s*(?=[0-9]+.){0,1}")?.Value;
                    entity.Inn = Regex.Match(data, @"(?<=ИНН:)\s*.*\s*(?=[0-9]+.){0,1}")?.Value;
                    entity.IsResident = Regex.Match(data, @"(?<=Резидент РФ:)\s*.*\s*(?=[0-9]+.){0,1}")?.Value;
                    entity.Leadership = Regex.Match(data, @"(?<=Руководство:)\s*.*\s*(?=[0-9]+.){0,1}")?.Value;
                    entity.Is7VZN = Regex.Match(data, @"(?<=Участник программы 7 ВЗН:)\s*.*\s*(?=[0-9]+.){0,1}")?.Value;
                    entity.Id = Regex.Match(data, @"(?<=Регистрационный номер участника:)\s*.*\s*(?=[0-9]+.){0,1}")?.Value;
                }

                table.Add(entity);
            }

            return table;
        }

        protected async Task<List<ITableEntity>> GetPhxCollection()
        {
            ICollection<CustomField> customFields;
            List<ITableEntity> table = new List<ITableEntity>();
            string[] ports, vers, omsStatus, mdlpStatus, type, hasOms;
            string backup, rdpConnection;
            int length;

            var assets = (await client.Asset_GetAgentsbyViewIdAsync(PharmXView, client.Authorization)).Result;

            foreach (var asset in assets)
            {
                try { customFields = (await client.Asset_GetCustomFields2Async(asset.AgentId.Value, client.Authorization)).Result; }
                catch { continue; }

                ports = customFields.FirstOrDefault((field) => field.FieldName == "PHX-Ports")?.FieldValue.Split(",");
                vers = customFields.FirstOrDefault((field) => field.FieldName == "PHX-Ver")?.FieldValue.Split(",");
                mdlpStatus = customFields.FirstOrDefault((field) => field.FieldName == "PHX-check-mdlp-conn")?.FieldValue.Split(",");
                omsStatus = customFields.FirstOrDefault((field) => field.FieldName == "PHX-check-oms-ping")?.FieldValue.Split(",");
                type = customFields.FirstOrDefault((field) => field.FieldName == "PHX-Stand-Type")?.FieldValue.Split(",");
                backup = customFields.FirstOrDefault((field) => field.FieldName == "PHX-Backup-State")?.FieldValue;
                hasOms = customFields.FirstOrDefault((field) => field.FieldName == "PHX-Has-OMS")?.FieldValue.Split(",");
                rdpConnection = customFields.FirstOrDefault((field) => field.FieldName == "RDP-connection")?.FieldValue;

                if (vers == null || ports == null)
                    continue;

                length = vers == null ? ports.Length : ports == null ? vers.Length : vers.Length > ports.Length ? vers.Length : ports.Length;

                for (int i = 0; i < length; i++)
                {
                    var entity = new PharmX();

                    entity.ServerName = asset.ComputerName;

                    if (mdlpStatus != null && i < mdlpStatus.Length)
                        entity.MDLPStatus = mdlpStatus[i];
                    else entity.MDLPStatus = "unknown";

                    if (omsStatus != null && i < omsStatus.Length)
                        entity.OMSStatus = omsStatus[i];
                    else entity.OMSStatus = "unknown";

                    if (vers != null && i < vers.Length)
                        entity.Version = vers[i];
                    else entity.Version = "unknown";

                    if (ports != null && i < ports.Length)
                        entity.Port = ports[i];
                    else entity.Port = "unknown";

                    if (type != null && i < type.Length)
                        entity.StandType = type[i].Trim() == StandType.Release ? StandType.RuRelease :
                              type[i].Trim() == StandType.Test ? StandType.RuTest : "unknown";
                    else entity.StandType = "unknown";

                    if (hasOms != null && i < hasOms.Length)
                        entity.HasOms = hasOms[i].Trim() == "True" ? true : false;

                    entity.InnerIp = asset.IPAddress;
                    entity.ConnectionGateway = string.IsNullOrEmpty(rdpConnection) ? asset.ConnectionGatewayIP : rdpConnection;
                    entity.Backup = backup;

                    table.Add(entity);
                }
            }

            return table;
        }

        protected async Task<List<ITableEntity>> GetTsdCollection()
        {
            var assets = new List<Agent>();
            ICollection<CustomField> customFields;
            List<ITableEntity> table = new List<ITableEntity>();
            string[] ports, dbvers, status;
            string rdpConnection;
            int length;

            assets.AddRange
                ((await client.Asset_GetAgentsbyViewIdAsync(CleverenceInstalledView, client.Authorization)).Result);

            foreach (var asset in assets)
            {
                try { customFields = (await client.Asset_GetCustomFields2Async(asset.AgentId.Value, client.Authorization)).Result; }
                catch { continue; }

                dbvers = customFields.FirstOrDefault((field) => field.FieldName == "PHX-Clev-DB-Ver")?.FieldValue.Split(",");
                ports = customFields.FirstOrDefault((field) => field.FieldName == "PHX-Clev-Ports")?.FieldValue.Split(",");
                status = customFields.FirstOrDefault((field) => field.FieldName == "PHX-Clev-Status")?.FieldValue.Split(",");
                rdpConnection = customFields.FirstOrDefault((field) => field.FieldName == "RDP-connection")?.FieldValue;

                if (dbvers == null && ports == null)
                    continue;

                length = dbvers == null ? ports.Length : ports == null ? dbvers.Length : dbvers.Length > ports.Length ? dbvers.Length : ports.Length;

                for (int i = 0; i < length; i++)
                {
                    var entity = new TSD();

                    entity.ServerName = asset.ComputerName;
                    entity.Version = customFields.FirstOrDefault((field) => field.FieldName == "PHX-Clev-Ver")?.FieldValue;

                    if (string.IsNullOrEmpty(entity.Version))
                        continue;

                    if (dbvers != null && i < dbvers.Length)
                        entity.DBVersion = dbvers[i];
                    else continue;

                    if (ports != null && i < ports.Length)
                        entity.Port = ports[i];
                    else entity.Port = "unknown";

                    if (status != null && i < status.Length)
                        entity.Status = status[i];
                    else entity.Status = "unknown";

                    entity.InnerIp = asset.IPAddress;
                    entity.ConnectionGateway = string.IsNullOrEmpty(rdpConnection) ? asset.ConnectionGatewayIP : rdpConnection;

                    table.Add(entity);
                }
            }

            return table;
        }

        protected List<ITableEntity> GetTroubleCollection()
        {
            List<ITableEntity> table = new List<ITableEntity>();
            Trouble trouble;

            foreach (var org in OrgTable)
            {
                if (org.HasProblem)
                {
                    trouble = new Trouble();
                    trouble.OrgName = org.ShortName;
                    trouble.MdlpHasProblem = org.PharmX.Any((item) => item.HasMdlpProblem);
                    trouble.OmsHasProblem = org.PharmX.Any((item) => item.HasOmsProblem);
                    trouble.TsdHasProblem = org.TSD.Any((item) => item.HasProblem);
                    trouble.BackupHasProblem = org.PharmX.Any((item) => item.HasBackupProblem);
                    table.Add(trouble);
                }
            }

            return table;
        }

        private IEnumerable<ITableEntity> GetPharmXRange(string urls, ICollection<ITableEntity> pharmXCollection)
        {
            if (string.IsNullOrEmpty(urls))
                return new PharmX[0];

            IEnumerable<ITableEntity> result = null;

            foreach (var url in urls.Split(','))
            {
                if (string.IsNullOrEmpty(url))
                    continue;

                result = result == null ? pharmXCollection.Where((item) =>
                 {
                     var pharmX = item as PharmX;
                     return url.Contains(pharmX.InnerIp.Trim() + ':' + pharmX.Port.Trim()) ||
                     url.Contains(pharmX.InnerIp.Trim() + ':' + pharmX.Port.Trim());
                 }) : result.Concat(pharmXCollection.Where((item) =>
                 {
                     var pharmX = item as PharmX;
                     return url.Contains(pharmX.InnerIp.Trim() + ':' + pharmX.Port.Trim()) ||
                     url.Contains(pharmX.InnerIp.Trim() + ':' + pharmX.Port.Trim());
                 }));
            }

            return result;
        }

        private IEnumerable<ITableEntity> GetTSDRange(string urls, ICollection<ITableEntity> tsdCollection)
        {
            if (string.IsNullOrEmpty(urls))
                return new TSD[0];

            IEnumerable<ITableEntity> result = null;

            foreach (var url in urls.Split(','))
            {
                if (string.IsNullOrEmpty(url))
                    continue;

                result = result == null ? tsdCollection.Where((item) =>
                {
                    var tsd = item as TSD;
                    return url.Contains(tsd.InnerIp.Trim() + ':' + tsd.Port.Trim()) ||
                    url.Contains(tsd.InnerIp.Trim() + ':' + tsd.Port.Trim());
                }) : result.Concat(tsdCollection.Where((item) =>
                {
                    var tsd = item as TSD;
                    return url.Contains(tsd.InnerIp.Trim() + ':' + tsd.Port.Trim()) ||
                    url.Contains(tsd.InnerIp.Trim() + ':' + tsd.Port.Trim());
                }));
            }

            return result;
        }

        protected override void ApplyChangesFrom(List<ITableEntity> table)
        {
            Table.Clear();
            OrgTable.Clear();
            PharmXTable.Clear();
            TsdTable.Clear();
            TroubleTable.Clear();

            PharmXReleaseTable.Clear();
            PharmXTestTable.Clear();
            TsdReleaseTable.Clear();
            TsdTestTable.Clear();

            Table = table;
            OrgTable = Table.OfType<Organization>().ToList();
            PharmXTable = Table.OfType<PharmX>().ToList();
            TsdTable = Table.OfType<TSD>().ToList();
            TroubleTable = GetTroubleCollection().Cast<Trouble>().ToList();

            foreach (PharmX item in PharmXTable)
            {
                switch (item.StandType)
                {
                    case StandType.Release:
                    case StandType.RuRelease:
                        PharmXReleaseTable.Add(item);
                        break;
                    case StandType.Test:
                    case StandType.RuTest:
                        PharmXTestTable.Add(item);
                        break;

                }
            }
            foreach (Organization org in OrgTable)
            {
                for (int i = 0; i < org.PharmX.Count; i++)
                {
                    if (i >= org.TSD.Count)
                        continue;

                    switch (org.PharmX[i].StandType)
                    {
                        case StandType.Release:
                        case StandType.RuRelease:
                            TsdReleaseTable.Add(org.TSD[i]);
                            break;
                        case StandType.Test:
                        case StandType.RuTest:
                            TsdTestTable.Add(org.TSD[i]);
                            break;
                        default:
                            org.TSD[i].StandType = "unknown";
                            continue;
                    }

                    org.TSD[i].StandType = org.PharmX[i].StandType;
                }
            }
        }
    }
}
