﻿using PharmXStatusDashboard.BaseAbstractions;
using System.Collections.Generic;
using System.Linq;

namespace PharmXStatusDashboard.TableEntities
{
    public class Organization : ITableEntity
    {
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public string Inn { get; set; }
        public string IsResident { get; set; }
        public string Leadership { get; set; }
        public string Is7VZN { get; set; }
        public string Id { get; set; }
        public string TSDType { get; set; }

        public string PhxUrls { get; set; }
        public string TsdUrls { get; set; }

        public List<PharmX> PharmX { get; private set; } = new List<PharmX>();
        public List<TSD> TSD { get; private set; } = new List<TSD>();

        public bool HasProblem => this.PharmX.Any((item) => item.HasProblem)
            || this.TSD.Any((item) => item.HasProblem);

        public bool Equals(ITableEntity tableEntity)
        {
            var org = tableEntity as Organization;

            if (org == null)
                return false;

            return ShortName == org.ShortName &&
                FullName == org.FullName &&
                Inn == org.Inn &&
                IsResident == org.IsResident &&
                Leadership == org.Leadership &&
                Is7VZN == org.Is7VZN &&
                Id == org.Id &&
                TSDType == org.TSDType &&
                PhxUrls == org.PhxUrls &&
                TsdUrls == org.TsdUrls &&
                TsdUrls == TsdUrls;
        }
    }
}
