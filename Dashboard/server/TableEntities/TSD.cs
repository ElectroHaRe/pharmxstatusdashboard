﻿using PharmXStatusDashboard.BaseAbstractions;

namespace PharmXStatusDashboard.TableEntities
{
    public class TSD : ITableEntity
    {
        public string InnerIp { get; set; }
        public string PublicIp { get; set; }
        public string ConnectionGateway { get; set; }
        public string Port { get; set; }
        public string Version { get; set; }
        public string DBVersion { get; set; }
        public string ServerName { get; set; }
        public string Status { get; set; }
        public string OrgName => Organization?.ShortName ?? "unknown";
        public string Type => Organization?.TSDType ?? "unknown";
        public string StandType { get; set; } = "unknown";

        public Organization Organization { get; set; }

        public bool HasProblem => Status != StatusType.GoodStatus;

        public bool Equals(ITableEntity tableEntity)
        {
            var tsd = tableEntity as TSD;

            if (tsd == null)
                return false;

            return InnerIp == tsd.InnerIp &&
                PublicIp == tsd.PublicIp &&
                ConnectionGateway == tsd.ConnectionGateway &&
                Port == tsd.Port &&
                Version == tsd.Version &&
                DBVersion == tsd.DBVersion &&
                Version == tsd.Version &&
                ServerName == tsd.ServerName &&
                Status == tsd.Status;
        }
    }
}
