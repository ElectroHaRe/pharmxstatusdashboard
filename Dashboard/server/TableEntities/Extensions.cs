﻿using System;
using System.Linq;

namespace PharmXStatusDashboard.TableEntities
{
    public static class StringExtension
    {
        public static string AddLines(this string source, params string[] lines)
        {
            if (lines == null || lines.Length == 0)
                return string.Empty;

            foreach (var line in lines)
            {
                if (line == null)
                    continue;
                source += Environment.NewLine + line.Trim();
            }

            return source?.Trim(Environment.NewLine.ToCharArray()) ?? string.Empty;
        }

        public static string RemoveLines(this string source, params string[] lines)
        {
            foreach (var line in lines)
            {
                if (source.Contains(line))
                    source.Remove(source.IndexOf(line), line.Count());
            }

            return source.Trim(Environment.NewLine.ToCharArray());
        }

        public static string[] GetLines(this string source)
        {
            return source.Split(',');
        }
    }
}
