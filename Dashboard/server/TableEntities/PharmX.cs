﻿using PharmXStatusDashboard.BaseAbstractions;
using System;

namespace PharmXStatusDashboard.TableEntities
{
    public class PharmX : ITableEntity
    {
        public string InnerIp { get; set; }
        public string PublicIp { get; set; }
        public string ConnectionGateway { get; set; }
        public string Port { get; set; }
        public string Version { get; set; }
        public string ServerName { get; set; }
        public string MDLPStatus { get; set; }
        public string OMSStatus { get; set; }
        public string Backup { get; set; }
        public string StandType { get; set; }
        public bool HasOms { get; set; }

        public string OrgName => Organization?.ShortName ?? "unknown";

        public Organization Organization { get; set; }

        public bool HasProblem => IsDown || HasOmsProblem || HasMdlpProblem || HasBackupProblem;

        public bool IsDown => Version?.Contains("down") ?? false;
        public bool HasOmsProblem => string.IsNullOrEmpty(OMSStatus)? false :
                                      OMSStatus == StatusType.BadStatus && !HasOms ? false :
                                      OMSStatus == StatusType.GoodStatus && HasOms ? false : true;
        public bool HasMdlpProblem => string.IsNullOrEmpty(MDLPStatus) ? true : 
                                      MDLPStatus == StatusType.BadStatus ? true : 
                                      MDLPStatus == StatusType.GoodStatus ? false : true;
        public bool HasBackupProblem => Backup != DateTime.Now.ToString("yyyyMMdd");

        public bool Equals(ITableEntity tableEntity)
        {
            var pharmX = tableEntity as PharmX;

            if (pharmX == null)
                return true;

            return InnerIp == pharmX.InnerIp &&
                PublicIp == pharmX.PublicIp &&
                ConnectionGateway == pharmX.ConnectionGateway &&
                Port == pharmX.Port &&
                Version == pharmX.Version &&
                ServerName == pharmX.ServerName &&
                MDLPStatus == pharmX.MDLPStatus &&
                OMSStatus == pharmX.OMSStatus &&
                Backup == pharmX.Backup &&
                HasOms == pharmX.HasOms &&
                StandType == pharmX.StandType;
        }
    }
}
