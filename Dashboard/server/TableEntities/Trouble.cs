﻿using PharmXStatusDashboard.BaseAbstractions;

namespace PharmXStatusDashboard.TableEntities
{
    public class Trouble : ITableEntity
    {
        public string OrgName { get; set; }
        public bool MdlpHasProblem { get; set; }
        public bool OmsHasProblem { get; set; }
        public bool TsdHasProblem { get; set; }
        public bool BackupHasProblem { get; set; }

        public bool Equals(ITableEntity tableEntity)
        {
            var trouble = tableEntity as Trouble;

            if (trouble == null)
                return false;

            return OrgName == trouble.OrgName &&
                MdlpHasProblem == trouble.MdlpHasProblem &&
                OmsHasProblem == trouble.OmsHasProblem &&
                TsdHasProblem == trouble.TsdHasProblem &&
                BackupHasProblem == trouble.BackupHasProblem;
        }
    }
}
