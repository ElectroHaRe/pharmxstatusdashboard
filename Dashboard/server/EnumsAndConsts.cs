﻿namespace PharmXStatusDashboard
{
    public struct StatusType
    {
        public const string GoodStatus = "0";
        public const string BadStatus = "1";
    }

    public struct StandType
    {
        public const string Release = "Release";
        public const string Test = "Test";

        public const string RuRelease = "Бой";
        public const string RuTest = "Тест";
    }
}
