﻿using Kaseya;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace PharmXStatusDashboard.BaseAbstractions
{
    public static class TableExension
    {
        public static bool EqualsTable(this List<ITableEntity> source, List<ITableEntity> table)
        {
            if (source.Count != table.Count)
                return false;

            for (int i = 0; i < source.Count; i++)
            {
                if (!source[i].Equals(table[i]))
                    return false;
            }

            return true;
        }

    }

    public abstract class TableService : IDisposable
    {
        protected readonly CancellationTokenSource UpdateCancelationToken = new CancellationTokenSource();

        public Action TableChanged;
        public TimeSpan UpdateInterval { get; private set; }
        public List<ITableEntity> Table { get; protected set; }

        protected Client client;

        public TableService(TimeSpan updateInterval)
        {
            Initialize(updateInterval);
        }

        protected void Initialize(TimeSpan updateInterval)
        {
            UpdateInterval = updateInterval;
            var clientCreating = Task.Run(async () => client = await Client.CreateInstance(new HttpClient()));
            clientCreating.Wait();
            var tableGenerating = Task.Run(async () => Table = await GenerateTable());
            tableGenerating.Wait();
            ApplyChangesFrom(tableGenerating.Result);
            Task.Run(async () => await Update(UpdateCancelationToken.Token));
            TableChanged?.Invoke();
        }

        protected abstract Task<List<ITableEntity>> GenerateTable();

        protected async Task Update(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                if (client.Token.SessionExpiration + TimeSpan.FromMinutes(30) > DateTime.Now)
                    await client.UpdateToken();

                var newTable = await GenerateTable();

                if (!Table.EqualsTable(newTable))
                {
                    ApplyChangesFrom(newTable);
                    TableChanged?.Invoke();
                }

                GC.Collect();
                await Task.Delay(UpdateInterval);
            }
        }

        protected virtual void ApplyChangesFrom(List<ITableEntity> table)
        {
            for (int i = 0; i < (Table.Count > table.Count ? Table.Count : table.Count); i++)
            {
                if (!Table[i].Equals(table[i]))
                    Table[i] = table[i];
            }

            if (Table.Count > table.Count)
                Table.RemoveRange(table.Count, Table.Count - table.Count);

            if (Table.Count < table.Count)
                Table.AddRange(table.TakeLast(table.Count - Table.Count));
        }

        public void Dispose()
        {
            UpdateCancelationToken.Cancel();
        }
    }
}
