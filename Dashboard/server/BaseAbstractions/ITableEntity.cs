﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PharmXStatusDashboard.BaseAbstractions
{
    public interface ITableEntity
    {
        bool Equals(ITableEntity tableEntity);
    }
}
